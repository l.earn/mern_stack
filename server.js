import express from 'express'; // hapi, sails, koa, pug(jade), handlebars
import fs from 'fs';
import sassMiddleware from 'node-sass-middleware';
import path from 'path';
import bodyParser from 'body-parser';

import config from './config';
import apiRouter from './api';

const server = express();

server.set('view engine', 'ejs');

server.use(sassMiddleware({
	src: path.join(__dirname, 'sass'),
	dest: path.join(__dirname, 'public')
}));

server.use(bodyParser.json());

import serverRender from './serverRender';

server.get(['/', '/contest/:contestId'], (req, res) => {
	serverRender(req.params.contestId)
		.then(({initialMarkup, initialData}) => {
			res.render('index', {
				initialMarkup,
				initialData
			});
		})
		.catch(error => {
			console.error(error);
			res.status(404).send(req.params.contestId + ' Not Found');
		});
});

// server.get('/about', (req, res) => {
// 	res.send('About\n');
// });

server.use('/api', apiRouter);
server.use(express.static('public'));

server.get('/config', (req, res) => {
	fs.readFile('./config.js', (err, data) => {
		res.send(data.toString());
	});
});

server.listen(config.port, config.host, () => {
	console.info('Express listening on port', config.port);
});
