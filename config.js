const env = process.env;

export default {
	host: env.HOST || '0.0.0.0',
	port: env.PORT || 8080,
	get serverUrl() {
		return `http://${this.host}:${this.port}`;
	},
	mongodbUri: 'mongodb://127.0.0.1:27017/test'
};